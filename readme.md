# Start the app 
`./manage.py runserver`

# Insert data into your app
Either by creating a super user and accessing the admin page
or by directly inserting in the database.

# Making queries
Go to http://127.0.0.1:8000/graphql/

# Simple query
```{
  allBooks {
    id
    title
  }
}
```
